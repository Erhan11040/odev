﻿using Uniport.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;

namespace Uniport.Controllers
{
    public class HomeController : Controller
    {

        
        public ActionResult Index()
        {
            uniportEntities3 db = new uniportEntities3();
            anasayfaresim res = new anasayfaresim();
            res.resim = db.urunler.ToList();
            return View(res);
        }
        public ActionResult urundetay()
        {
            uniportEntities3 db = new uniportEntities3();
            satis sat = new satis();
            sat.urun = db.urunler.ToList();
            var makaleId = Request.QueryString["id"];
            Session["detay"] = makaleId;
            return View(sat);
        }

        public ActionResult sepeteekle()
        {

            return View();
        }
        public ActionResult adminsayfasi()
        {

            return View();
        }
        [HttpPost]
        public ActionResult sepeteekle(FormCollection form)
        {
            uniportEntities3 db = new uniportEntities3();
            sepet ol = new sepet();
            ol.fiyat = Decimal.Parse(form["fiyat"].Trim()); 
            ol.email = Session["email"].ToString() ;
            ol.adet = Int32.Parse(form["adet"].Trim()); 
            ol.urunno = Int32.Parse(form["urunno"].Trim()); 
            ol.urunadi = form["urunadi"].Trim(); 
            ol.url = form["url"].Trim(); 
            db.sepet.Add(ol);
            db.SaveChanges();
            return View();
        }
        [HttpPost]
        public ActionResult ilanver(FormCollection form, HttpPostedFileBase uploadfile)
        {
            uniportEntities3 db = new uniportEntities3();
            urunler ur = new urunler();
            ur.urunadi = form["urunadi"].Trim();
            ur.bilgi = form["bilgi"].Trim();
            ur.url = "/resim/"+uploadfile.FileName;
            ur.fiyat = Decimal.Parse(form["fiyat"].Trim());
            db.urunler.Add(ur);
            if (uploadfile.ContentLength > 0)
            {
                string filePath = Path.Combine(Server.MapPath("~/resim"), Guid.NewGuid().ToString() + "_" + Path.GetFileName(uploadfile.FileName));
                uploadfile.SaveAs(filePath);
            }
            db.SaveChanges();
            return View();
        }
        public ActionResult cikis()
        {
            Session.Clear();
            return View();
        }
        [HttpPost]
        public ActionResult Index(kullanicilar Model,FormCollection form)
        {
            uniportEntities3 db = new uniportEntities3();
            var kullanici = db.kullanicilar.FirstOrDefault(x=>x.email == Model.email && x.sifre == Model.sifre);
            var admin = db.kullanicilar.FirstOrDefault(x=>x.email ==Model.email && x.sifre ==Model.sifre && x.id == 1);
            string kullaniciadi = form["email"].Trim();
            string sifre = form["sifre"].Trim();
            if (kullaniciadi=="erhan"  && sifre=="123123")
            {
               
                
                    Session["email"] = kullanici;
                    return RedirectToAction("adminsayfasi", "Home");
                

            }else if(kullanici != null)
            {
                Session["email"] = kullanici;
                return RedirectToAction("Index", "Home");
            }
            kisiler o1 = new kisiler();
            o1.isim = db.kullanicilar.ToList();

            return View(o1);
        }
       
        public ActionResult ilanlar()
        {
            uniportEntities3 db = new uniportEntities3();
            satis sat = new satis();
            sat.urun = db.urunler.ToList();
            return View(sat);
        }
        public ActionResult sepetim()
        {
            uniportEntities3 db = new uniportEntities3();
            sepet1 s = new sepet1();
            s.sep = db.sepet.ToList();
            return View(s);
        }
        public ActionResult ilanver()
        {
            return View();
        }
   
        public ActionResult satistakiler()
        {
            uniportEntities3 db = new uniportEntities3();
            satis sat = new satis();
            sat.urun = db.urunler.ToList();
            return View(sat);
        }
        public ActionResult hakkimizda()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        public ActionResult kaydol()
        {
            return View();
        }
        [HttpPost]
        public ActionResult kaydol(FormCollection form)
        {
            uniportEntities3 db = new uniportEntities3();
            kullanicilar kul = new kullanicilar();
            kul.isim = form["isim"].Trim();
            kul.telefon = form["tel"].Trim();
            kul.sifre = form["sifre"].Trim();
            kul.email = form["email"].Trim();
            kul.adres = form["adres"].Trim();
            db.kullanicilar.Add(kul);
            db.SaveChanges();
            return View();
        }
       
        public class kisiler
        {
            public List <kullanicilar> isim { get; set; }
        }
        public class anasayfaresim
        {
            public List<urunler> resim { get; set; }
        }
        public class satis
        {
            public List<urunler> urun { get; set; }
        }
        public class sepet1
        {
            public List<sepet> sep { get; set; }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Uniport.Models;

namespace Uniport.Controllers
{
    public class AdminController : Controller
    {
        private uniportEntities3 db = new uniportEntities3();

        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult kullaniciIndex()
        {
            return View(db.kullanicilar.ToList());
        }

        // GET: Admin/Details/5
        public ActionResult kullaniciDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            kullanicilar kullanicilar = db.kullanicilar.Find(id);
            if (kullanicilar == null)
            {
                return HttpNotFound();
            }
            return View(kullanicilar);
        }

        // GET: Admin/Create
        public ActionResult kullaniciCreate()
        {
            return View();
        }

        // POST: Admin/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult kullaniciCreate([Bind(Include = "id,isim,email,telefon,sifre,adres")] kullanicilar kullanicilar)
        {
            if (ModelState.IsValid)
            {
                db.kullanicilar.Add(kullanicilar);
                db.SaveChanges();
                return RedirectToAction("kullaniciIndex");
            }

            return View(kullanicilar);
        }

        // GET: Admin/Edit/5
        public ActionResult kullaniciEdit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            kullanicilar kullanicilar = db.kullanicilar.Find(id);
            if (kullanicilar == null)
            {
                return HttpNotFound();
            }
            return View(kullanicilar);
        }

        // POST: Admin/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult kullaniciEdit([Bind(Include = "id,isim,email,telefon,sifre,adres")] kullanicilar kullanicilar)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kullanicilar).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("kullaniciIndex");
            }
            return View(kullanicilar);
        }

        // GET: Admin/Delete/5
        public ActionResult kullaniciDelete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            kullanicilar kullanicilar = db.kullanicilar.Find(id);
            if (kullanicilar == null)
            {
                return HttpNotFound();
            }
            return View(kullanicilar);
        }

        // POST: Admin/Delete/5
        [HttpPost, ActionName("kullaniciDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            kullanicilar kullanicilar = db.kullanicilar.Find(id);
            db.kullanicilar.Remove(kullanicilar);
            db.SaveChanges();
            return RedirectToAction("kullaniciIndex");
        }

    }
}
